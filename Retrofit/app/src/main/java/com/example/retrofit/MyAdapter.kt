package com.example.retrofit

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit.databinding.FragmentListBinding
import com.example.retrofit.databinding.RowItemsBinding

class MyAdapter(val dataModel: DataModel, val listBinding: FragmentListBinding, val userList: List<MyDataItem>): RecyclerView.Adapter<MyAdapter.ViewHolder>(){

    class ViewHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = RowItemsBinding.bind(item)

        fun bind(myDataItem: MyDataItem, dataModel: DataModel, listBinding: FragmentListBinding) = with(binding){
            tvBody.text = myDataItem.body
            tvTitle.text = myDataItem.title
            linLayout.setOnClickListener{
                dataModel.curTitle.value = myDataItem
                Navigation.findNavController(listBinding.root).navigate(R.id.nav_listFragm_to_itemFrag)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_items, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position], dataModel, listBinding)
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}