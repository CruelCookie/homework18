package com.example.retrofit

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiInterface {

    @POST("posts")
    fun updateUser(@Body myDataItem: MyDataItem):Call<MyDataItem>

    @GET("posts")
    fun getData(): Call<List<MyDataItem>>
}