package com.example.retrofit

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.retrofit.databinding.FragmentItemBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemFragment : Fragment() {
    private lateinit var binding: FragmentItemBinding
    private val dataModel:DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentItemBinding.inflate(inflater)
        val dataItem = dataModel.curTitle.value!!
        
        binding.etTitle.setText(dataItem.title)
        binding.etName.setText(dataItem.body)
        
        binding.btClose.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.nav_itemFrag_to_listFrag)
        }
        binding.btUpdate.setOnClickListener {
            dataModel.curTitle.value!!.title = binding.etTitle.text.toString()
            dataModel.curTitle.value!!.body = binding.etName.text.toString()
            dataModel.retrofitBuilder.value!!.updateUser(dataModel.curTitle.value!!).enqueue(object : Callback<MyDataItem?> {
                override fun onResponse(call: Call<MyDataItem?>, response: Response<MyDataItem?>) {
                    Log.i(TAG,response.body().toString())
                }

                override fun onFailure(call: Call<MyDataItem?>, t: Throwable) {
                    Log.i(TAG,t.message!!)
                }
            })
            Navigation.findNavController(binding.root).navigate(R.id.nav_itemFrag_to_listFrag)
        }
        return binding.root
    }

    companion object {
        const val TAG = "ItemFragment"
        @JvmStatic
        fun newInstance() = ItemFragment()
    }
}