package com.example.retrofit

data class MyDataItem(
    var body: String,
    var id: Int,
    var title: String,
    val userId: Int
)