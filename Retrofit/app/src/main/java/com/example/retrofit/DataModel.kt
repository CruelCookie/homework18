package com.example.retrofit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Retrofit

class DataModel: ViewModel(){
    val curTitle: MutableLiveData<MyDataItem> by lazy{
        MutableLiveData<MyDataItem>()
    }
    val retrofitBuilder: MutableLiveData<ApiInterface> by lazy{
        MutableLiveData<ApiInterface>()
    }
}
